#!/bin/bash
set -uo pipefail

# Some (all?) installations of Debian/Ubuntu OS include backports packages? Can we expect people to have backport packages configured? Is this even relevant for us? 
# Therefore,
# 1) get the depencendies of bitmask-vpn
# 3) check if one of these packages is part of backports

# shows all dependency packages of bitmask-vpn binary
print_dependency_packages() {
	for file in $(ldd /usr/bin/bitmask-vpn | awk '{print $1 }' | grep -v -e vdso.so)
	do
		PACKAGE=$(dpkg -S $file | head -n 1 | cut -d: -f1)
		#echo -e "Doing file$file\n\n$PACKAGE"
		echo $PACKAGE
	done
}

# checks if a package is part of backports
check_if_package_is_in_backports() {
	# Debian 12
	# wget http://ftp.am.debian.org/debian/dists/bookworm-backports/main/binary-all/Packages.xz; unxz Packages.xz
	# Ubuntu 24.04
	# wget https://mirrors.dc.clear.net.ar/ubuntu/dists/noble-backports/universe/binary-amd64/Packages.xz; unxz Packages.xz; mv Packages Packages-universe
	# wget https://mirrors.dc.clear.net.ar/ubuntu/dists/noble-backports/main/binary-amd64/Packages.xz; unxz Packages.xz; mv Packages Packages-main
	# Ubuntu 22.04
	# wget https://mirrors.dc.clear.net.ar/ubuntu/dists/jammy-backports/universe/binary-amd64/Packages.xz; unxz Packages.xz; mv Packages Packages-universe
	# wget https://mirrors.dc.clear.net.ar/ubuntu/dists/jammy-backports/main/binary-amd64/Packages.xz; unxz Packages.xz; mv Packages Packages-main
	for package in $(cat deps-dedup.txt)
	do
		# check if its in main
		#apt-cache show $package | grep Filename
		echo "Checking if package $package is in main/universe backports"
		cat Packages* | grep ^Package | grep $package

	done
}

# first, run
# ./check-if-dep-is-in-backports.sh | sort | uniq > deps-dedup.txt
print_dependency_packages

# then run it with this function
#check_if_package_is_in_backports
