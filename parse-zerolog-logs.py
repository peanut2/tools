#!/usr/bin/env python3
import json
import sys
from pathlib import Path


"""
1) In bitmask-vpn we use github.com/rs/zerolog
2) Our central log file is ~/.config/leap/systray.json
3) But zerolog writes it as json ...
4) People create issues and set json logs
5) This peace of converts the json log into a readable format


echo '{"level":"info","updateAvailable":false,"time":"2024-07-02T23:12:01+02:00","message":"Sucessfully checked if there is an update"}' > log.json

python parse-zerolog-logs.py log.json
2024-07-02T23:12:01+02:00 INFO updateAvailable="False" message="Sucessfully checked if there is an update"

"""


def fail(msg):
    print(msg)
    sys.exit(1)


def main():
    if len(sys.argv) != 2:
        fail(f"{sys.argv[0]} <log file with json>")

    file = Path(sys.argv[1])

    if not file.exists():
        fail(f"File {file} does not exist")

    for line in file.read_text().splitlines():
        data = json.loads(line)
        out = data['time'] + " " + data["level"].upper() + " "
        del data['time']
        del data['level']

        for key, value in data.items():
            out += f'{key}="{value}" '
        print(out)


if __name__ == '__main__':
    main()
