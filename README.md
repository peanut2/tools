# Tools

- calc-gitlab-hours.py - sum up time estimates of issues
- parse-zerolog-logs.py - parse zerolog log files (json) and make them readable
- check-if-dep-is-in-backports.sh - check if one of the dependencies is part of a backport packages - it seems like some installations have backports enabled (automatically)
- geolookup-ooni.py - Ask OONI api to get a country code for an ip address

More information can be found in the source code.
