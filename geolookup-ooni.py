import requests

url = "https://api.dev.ooni.io/api/v1/geolookup"
data = {"addresses": ["212.83.144.108"]}

req = requests.post(url, json=data)
req.raise_for_status()
print(req.text)
