#!/usr/bin/env python3
import re
import sys
from pathlib import Path

"""

1) Set time estimates in your issues and filter your issues:
https://0xacab.org/leap/bitmask-vpn/-/issues/?sort=created_date&state=all&assignee_username%5B%5D=peanut2&milestone_title=2024.08%20LEAP%20VPN%20Release&first_page_size=20
2) Think about: open|closed|all
3) Just copy the output the browser content (ctrl-a ctrl-c) - think about pagination
4) write it into a file
5) run the tool

pea@peabox:tools python calc-gitlab-hours.py
calc-gitlab-hours.py <file with gitlab output>

pea@peabox:tools python calc-gitlab-hours.py hours.txt
Adding hour: 2
Adding hour: 4
Adding hour: 2
Adding hour: 2
Adding hour: 1
Adding hour: 3
Adding hour: 3
Adding hour: 1
Adding hour: 3
Adding hour: 2
Adding hour: 2
Adding hour: 2
Adding hour (by week) 24
Total hours: 51h


can also be done with this: https://docs.gitlab.com/ee/user/project/time_tracking.html#global-time-tracking-report
but needs to be enabled in Gitlab
"""


def add_hours(text: str) -> int:
    total = 0
    matches = re.findall(r'\dh\s', text)
    for match in matches:
        hour = match.replace("h", "").strip()
        hour = int(hour)
        print("Adding hour:", hour)
        total += hour
    return total


def add_days(text: str) -> int:
    total = 0
    matches = re.findall(r'\dd\s', text)
    for match in matches:
        hour = match.replace("d", "").strip()
        hour = int(hour) * 8
        print("Adding hour (by week)", hour)
        total += hour
    return total


def main():
    total = 0
    if len(sys.argv) != 2:
        print(f"{sys.argv[0]} <file with gitlab output>")
        return

    text = Path(sys.argv[1]).read_text()
    total += add_hours(text)
    total += add_days(text)
    print(f"Total hours: {total}h")


if __name__ == '__main__':
    main()
